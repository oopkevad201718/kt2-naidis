import java.time.LocalDateTime;

public class VäliseMonitorigaArvuti extends Arvuti {
  public VäliseMonitorigaArvuti(String tootja, String kiirTöö, LocalDateTime registreerimiseAeg) {
    super(tootja, kiirTöö, registreerimiseAeg);
  }

  @Override public String toString() {
    return getTootja() + ";" + getTööLiik() + ";" + "monitoriga" + "@" + getRegistreerimiseAeg().toString();
  }
}
