import java.io.DataInputStream;

public class Töötaja {
  private String nimi;
  private double tunnitasu;

  public String getNimi() {
    return nimi;
  }

  public Töötaja(String nimi, double tunnitasu) {
    this.nimi = nimi;
    this.tunnitasu = tunnitasu;
  }

  public double arvutaTasu(int minuteid, Arvuti arvuti) {
    double hind = (minuteid / 60.0) * tunnitasu;

    if (arvuti.onKiirtöö()) {
      hind += 10;
    }

    if (arvuti instanceof VäliseMonitorigaArvuti) {
      hind += 1;
    }

    hind += 2;
    return Math.round(hind * 100) / 100;
  }

  public static Töötaja laadi(DataInputStream dis) throws Exception {
    String nimi = dis.readUTF();
    double tasu = dis.readDouble();
    return new Töötaja(nimi, tasu);
  }
}
