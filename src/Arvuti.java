import java.time.LocalDate;
import java.time.LocalDateTime;

public class Arvuti {
  private String tootja;
  private String tööLiik;
  private LocalDateTime registreerimiseAeg;

  public String getTootja(){
    return  tootja;
  }

  public String getTööLiik() {
    return tööLiik;
  }

  public LocalDateTime getRegistreerimiseAeg() {
    return registreerimiseAeg;
  }

  public Arvuti(String tootja, String tööLiik, LocalDateTime registreerimiseAeg) {
    this.tootja = tootja;
    this.tööLiik = tööLiik;
    this.registreerimiseAeg = registreerimiseAeg;
  }

  public boolean onKiirtöö() {
    return tööLiik.equals("kiirtöö");
  }

  @Override public String toString() {
    return tootja + ";" + tööLiik + "@" + registreerimiseAeg.toString();
  }

  //Abifunktsioonid
  public static boolean onValiidneTöö(String töö) {
    return töö.equals("tavatöö") || töö.equals("kiirtöö");
  }

  public static boolean onKiirTöö(String töö) {
    return töö.equals("kiirtöö");
  }

  public static boolean onMonitoriga(String monitoriga) {
    return monitoriga.equals("monitoriga");
  }

}
