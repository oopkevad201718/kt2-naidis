import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sessioon {
  private List<Arvuti> parandamata;
  private Map<Arvuti, Double> parandatud;
  private double teenitudTasu;

  public Sessioon() {
    parandamata = new ArrayList<>();
    parandatud = new HashMap<>();
    teenitudTasu = 0;
  }

  public double getTeenitudTasu() {
    return Math.round(teenitudTasu * 100) / 100;
  }

  public void lisaParandamataArvuti(Arvuti arvuti) {
    parandamata.add(arvuti);
  }

  public void arvutiParandatud(Arvuti a, double tasu) {
    parandamata.remove(parandamata.indexOf(a)); // Eemaldame parandatud arvuti parandamist vajavate hulgast.
    parandatud.put(a, tasu);
    this.teenitudTasu += tasu;
  }

  public Arvuti järgmineParandamistVajavArvuti() {
    if (parandamata.size() == 0) {
      return null;
    }

    //Tagasta esimene kiirtöö
    for (Arvuti a : parandamata) {
      if (a.onKiirtöö()) {
        return a;
      }
    }

    //Tagasta esimene tavatöö, kui kiirtöösid ei olnud
    return parandamata.get(0);
  }

  public void väljastaKokkuvõte() {
    System.out.println("Sessiooni kokkuvõte:");
    System.out.println("Teenitud raha: " + getTeenitudTasu() + "€");
    System.out.println("Parandatud arvuteid: ");

    Map<String, Integer> arvutid = new HashMap<>();
    for (Arvuti arvuti : parandatud.keySet()) {
      //Kui on hasmapis, suurendame arvu, kui ei ole lisame uue mappi ja arvutite arvuks saab 1.
      arvutid.merge(arvuti.getTootja(), 1, (a, b) -> a + b);
    }

    for (String tootja : arvutid.keySet()) {
      System.out.println("\t" + tootja + ": " + arvutid.get(tootja) + "tk");
    }
    System.out.println("Ootele jäi " + parandamata.size() + " arvuti(t).");
  }

  public void salvestaAndmed() throws IOException {
    //Tehtud tööd
    DataOutputStream dos = new DataOutputStream(new FileOutputStream("tehtud.dat"));

    dos.writeInt(parandatud.size()); // Arvutite hulk
    for(Arvuti arvuti : parandatud.keySet()) {
      dos.writeUTF(arvuti.getTootja()); // Tootja
      dos.writeUTF(arvuti.getRegistreerimiseAeg().toString()); //Aeg
      dos.writeDouble(parandatud.get(arvuti)); //Tasu
    }
    dos.close();

    //Tegemata tööd
    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("ootel.txt"), "UTF-8"));
    for (Arvuti arvuti: parandamata) {
      writer.write(arvuti.toString() + "\n");
    }
    writer.close();
  }
}
