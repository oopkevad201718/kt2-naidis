import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDateTime;

public class Arvutiparandus {

  public static void main(String[] args) throws Exception {
    String koht = args[0];
    InputStream sisse;
    Sessioon sessioon = new Sessioon();
    Töötajad töötajad = new Töötajad();
    töötajad.laadi("tunnitasud.dat"); //Laeme töötajad sisse töötajate failist

    // Loeme tööde nimekirja veebilehelt või failist
    if (koht.startsWith("http://") || koht.startsWith("https://"))
      sisse = new URL(koht).openStream();
    else
      sisse = new FileInputStream(koht);
    BufferedReader bf = new BufferedReader(new InputStreamReader(sisse, "UTF-8"));

    //Lisame tööd sessiooni
    String line = bf.readLine();
    while (line != null) {
      try {
        Arvuti a = loeArvuti(line);
        sessioon.lisaParandamataArvuti(a);
      } catch (FormaadiErind e) {
        System.out.println(e.getMessage());
      }
      line = bf.readLine();
    }

    //Kasutajalt info küsimine
    String response = "";
    while (true) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      System.out.println("Kas soovid parandada (P), uut tööd registreerida (R) või lõpetada (L) ?");
      response = reader.readLine();
      if (response.equals("P")) { // ARVUTI PARANDAMINE
        Arvuti parandada = sessioon.järgmineParandamistVajavArvuti();
        if (parandada == null) {
          System.out.println("Ühtegi arvutit ei ole hetkel parandada!");
        }
        else {
          System.out.println("Arvuti info: " + parandada.toString());

          int kulunudMinuteid;
          Töötaja töötaja;

          while (true) {
            try {
              System.out.println("Sisesta parandamiseks kulunud aeg (täisminutites): ");
              kulunudMinuteid = Integer.parseInt(reader.readLine());
              if (kulunudMinuteid < 0) {
                throw new RuntimeException();
              }
              break;
            } catch (Exception e) {
              System.out.println("Ebakorrektne sisend");
            }
          }

          while (true) {
            System.out.println("Sisesta enda nimi:"); //Jaan, Peeter, Tiina
            String name = reader.readLine();
            töötaja = töötajad.getTöötajaByName(name);
            if (töötaja != null) {
              break;
            }
            System.out.println("Ei leidnud sellist isikut");
          }

          double tasu = töötaja.arvutaTasu(kulunudMinuteid, parandada);
          sessioon.arvutiParandatud(parandada, tasu);
          System.out.println("Töö tehtud, arve summa: " + tasu + "€.");
        }
      }
      else if (response.equals("R")) { // UUE TÖÖ REGISTREERIMINE
        while (true) {
          System.out.println("Sisesta töö kirjeldus:");
          response = reader.readLine();
          try {
            Arvuti a = loeArvuti(response);
            sessioon.lisaParandamataArvuti(a);
            System.out.println("Töö on registreeritud!");
            break;
          } catch (FormaadiErind e) {
            System.out.println(e.getMessage());
          }
        }
      }
      else if (response.equals("L")) {
        sessioon.väljastaKokkuvõte();
        sessioon.salvestaAndmed();
        break;
      } else {
        System.out.println("Vigane sisend, proovi uuesti!");
      }
    }
  }

  private static Arvuti loeArvuti(String rida) {
    String[] andmedJaKuupäev = rida.split("@");
    String[] tykid = andmedJaKuupäev[0].split(";");

    LocalDateTime kp;
    if (andmedJaKuupäev.length == 2) { //Kuupäev oli olemas
      kp = LocalDateTime.parse(andmedJaKuupäev[1]);
    } else { //Kuupäeva ei olnud, järelikult on tegu käsitsi lisamisega
      kp = LocalDateTime.now();
    }

    if (tykid.length < 2 || tykid.length > 3) {
      throw new FormaadiErind("Semikoolonitega eraldatud väljade arv on väiksem, kui kaks, või suurem, kui kolm");
    }

    if (!Arvuti.onValiidneTöö(tykid[1])) {
      throw new FormaadiErind("Töö tüübi väljas olev väärtus ei ole \"tavatöö\" ega \"kiirtöö\"");
    }

    if (tykid.length == 3) { //Monitoriga

      if (!Arvuti.onMonitoriga(tykid[2])) {
        throw new FormaadiErind("väljade arv on kolm, aga kolmanda välja väärtus ei ole \"monitoriga\"");
      }

      return new VäliseMonitorigaArvuti(tykid[0], tykid[1], kp);
    }
    return new Arvuti(tykid[0], tykid[1], kp);
  }
}
