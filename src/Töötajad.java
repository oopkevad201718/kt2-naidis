import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class Töötajad {
  private List<Töötaja> töötajaList;

  public Töötajad() {
    this.töötajaList = new ArrayList<>();
  }

  public Töötaja getTöötajaByName(String name) {
    for(Töötaja t: töötajaList) {
      if (t.getNimi().equals(name)) {
        return t;
      }
    }
    return null;
  }

  public void laadi(String failinimi) throws Exception {
    DataInputStream dis = new DataInputStream(new FileInputStream(failinimi));

    int töötajaid = dis.readInt();
    for (int i = 0; i < töötajaid; i++) {
      töötajaList.add(Töötaja.laadi(dis));
    }

    dis.close();
  }
}
